FROM alpine
RUN apk add --no-cache curl git jq
RUN apk add --no-cache aws-cli
RUN apk add --no-cache terraform

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/bin/kubectl

RUN curl -sSL -o argocd-linux-amd64 https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
RUN install -m 555 argocd-linux-amd64 /usr/local/bin/argocd
RUN rm argocd-linux-amd64

RUN apk add --no-cache helm

RUN apk add --no-cache yq

RUN apk add --no-cache openssh-client
